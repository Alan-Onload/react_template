/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/reactjs.jsx to edit this template
 */
import React from 'react';
import { useSelector } from 'react-redux';
import './Users.css'

export default function () {

    const dados = useSelector((state) => state.user.dados)
    return(
            <div className="container d-flex justify-content-center">
                <div className="col-sm-6 col-md-10">
                <table className="table text-center mt-5">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>username</th>
                            <th>email</th>
                            <th>password</th>
                        </tr>           
                    </thead>
                    <tbody>
                        {
                            dados.map((ds, index) => {

                                if(index > 0)
                                return(
                                <tr>
                                    <td>{index}</td>
                                    <td>{ds.username}</td>
                                    <td>{ds.email}</td>
                                    <td>{ds.password}</td>
                                </tr>
                                )



                            })
                        }
            
                    </tbody>
                </table>
                </div>
            </div>
            )

}