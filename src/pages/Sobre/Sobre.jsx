/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/reactjs.jsx to edit this template
 */
import React from 'react';
import {useSelector} from "react-redux"

export default function(){
        var user = useSelector((state)=> state.user.user);        
        return(
                <div className="container">
                    <p className="text-center">{user.email}</p>
                </div>                
                
        )
}