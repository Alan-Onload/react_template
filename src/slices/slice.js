import {createSlice} from '@reduxjs/toolkit'

export const defaultSlice = createSlice({
    name:'user',
    initialState:{
        user:{
            username:"",
            email:"",
            password:""
        },
        dados:[{}]
        
    },
    reducers:{
        logar:(state,action)=>{
            state.user = action.payload;
            console.log(JSON.stringify(state.user))
        },
        salvar:(state,action)=>{
            state.dados.push(action.payload);
            console.table(state.dados)
        }
    }
    
})

export const {logar,salvar} = defaultSlice.actions
export default defaultSlice.reducer;