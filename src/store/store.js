import { configureStore } from '@reduxjs/toolkit';
import defaultSlice from '../slices/slice'

export default configureStore({
        reducer: {
            user:defaultSlice
        },
})


