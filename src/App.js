import "./App.css";
import { Routes, Route } from "react-router-dom";
import Navegador from './components/navegador/Navegador'
import Home from './pages/Home/Home';
import Users from './pages/Users/Users';
import Sobre from './pages/Sobre/Sobre';
function App() {
  return (
    <div className="App">
      <Navegador></Navegador>  
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/users" element={<Users />} />
        <Route path="/sobre" element={<Sobre />} />
      </Routes>
    </div>
  );
}

export default App;
