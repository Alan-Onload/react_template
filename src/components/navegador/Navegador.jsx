import React from 'react'
import {Link} from 'react-router-dom'
import {BsHouseFill,BsBookFill} from 'react-icons/bs'

export default function () {
    return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <a className="navbar-brand" to="/">Onload</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item d-flex align-items-center">
                                <BsHouseFill className="me-1"/><Link className="nav-link active" aria-current="page" to="/">Home</Link>
                            </li>
                            <li className="nav-item d-flex align-items-center">
                                <BsBookFill className="me-1"/><Link className="nav-link" to="/users">Users</Link>
                            </li>
                            <li className="nav-item d-flex align-items-center">
                                <BsBookFill className="me-1"/><Link className="nav-link" to="/sobre">Sobre</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            )
}

